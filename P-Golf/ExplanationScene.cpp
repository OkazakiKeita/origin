#include "ExplanationScene.h"
#include "Engine/Image.h"
//コンストラクタ
ExplanationScene::ExplanationScene(IGameObject * parent)
	: IGameObject(parent, "ExplanationScene"), alpha_(1.0f), alphaFlg_(false)
{
}

//初期化
void ExplanationScene::Initialize()
{
	hPict_ = Image::Load("Data/Image/Explanation.png");
	hPictBB_ = Image::Load("Data/Image/BlackBack.png");
}

//更新
void ExplanationScene::Update()
{
	//画面のフェード
	if (alphaFlg_ == false && alpha_ > 0.0f)
	{
		alpha_ -= 0.01f;
	}

	if (alpha_ <= 0)
	{
		if (Input::IsKeyDown(DIK_RETURN))
		{
			alphaFlg_ = true;
		}
	}

	//画面のフェード
	if (alphaFlg_ == true)
	{
		alpha_ += 0.01f;
		if (alpha_ >= 1.0f)
		{
			SceneManager::ChangeScene(SCENE_ID_PLAY);
		}
	}
}

//描画
void ExplanationScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);

	Image::SetMatrix(hPictBB_, worldMatrix_);
	Image::Draw(hPictBB_, alpha_);
}

//開放
void ExplanationScene::Release()
{
}