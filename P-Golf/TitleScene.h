#pragma once
#include "Engine/global.h"

//クラスの前方宣言
class Camera;

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
private:
	int hPict_;
	int hPictBB_;
	float alpha_;
	bool alphaFlg_;
public:
	//コンストラクタ
	//引数：parent
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};