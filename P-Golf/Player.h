#pragma once
#include "Engine/IGameObject.h"
#include "ShootGuide.h"

//プレイヤーを管理するクラス
class Player : public IGameObject
{
	int hModel_;

	ShootGuide* pShootGuide_;

	//回転可能か
	bool canRotate_;

	//打球可能状態に変化したか
	bool nextPat_;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};