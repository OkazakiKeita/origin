#pragma once
#include "Engine/global.h"
#include "Ball.h"
#include "Player.h"
#include "Stage1.h"
#include "Target.h"

//クラスの前方宣言
class Camera;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
private:
	Ball* pBall;
	Player* pPlayer;
	Stage1* pStage;
	Target* pTarget;

public:
	//コンストラクタ
	//引数：parent
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	Ball* GetBall()
	{
		return pBall;
	}
	Stage1* GetStage()
	{
		return pStage;
	}
	Target* GetTarget()
	{
		return pTarget;
	}
};