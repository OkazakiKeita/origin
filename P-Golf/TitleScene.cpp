#include "TitleScene.h"
#include "Engine/Image.h"
//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), alpha_(0.0f),alphaFlg_(false)
{
}

//初期化
void TitleScene::Initialize()
{
	hPict_ = Image::Load("Data/Image/Title.png");
	hPictBB_ = Image::Load("Data/Image/BlackBack.png");
}

//更新
void TitleScene::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		alphaFlg_ = true;
	}
	//画面のフェード
	if (alphaFlg_)
	{
		alpha_ += 0.01f;
		if (alpha_ > 1.0f)
		{
			SceneManager::ChangeScene(SCENE_ID_EXPLANATION);
		}
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
	
	Image::SetMatrix(hPictBB_, worldMatrix_);
	Image::Draw(hPictBB_, alpha_);
}

//開放
void TitleScene::Release()
{
}