#pragma once
#include "Engine/IGameObject.h"

//旗を管理するクラス
class Flag : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	Flag(IGameObject* parent);

	//デストラクタ
	~Flag();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//地面の位置にくっつける
	void FollowGround();
};