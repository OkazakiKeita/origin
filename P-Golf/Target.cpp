#include "Target.h"
#include "Engine/Model.h"
#include "PlayScene.h"

const D3DXVECTOR3 DEF_CAM_POS = D3DXVECTOR3(0, 10, -15);
const D3DXVECTOR3 TOP_CAM_POS = D3DXVECTOR3(0, 100, -1);
const int ZOOM_SPEED = 500;


//コンストラクタ
Target::Target(IGameObject * parent)
	:IGameObject(parent, "Target"), zoomVec(D3DXVECTOR3(0, 0, 0)), camPos(DEF_CAM_POS)
{
}

//デストラクタ
Target::~Target()
{
}

//初期化
void Target::Initialize()
{
	hModel_ = Model::Load("Data/Model/Shade.fbx");

	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetPosition(DEF_CAM_POS);

	position_ = ((PlayScene*)pParent_)->GetBall()->GetPosition();
	
	
	tagPos = pCamera_->GetTarget();

	//ゴールの位置を向くようにする
	//自分の位置とゴールの位置を引いてベクトル出して
	//回転あたりで使ったやつで角度求めてそれを自分の回転に入れる
	D3DXVECTOR2 baseXY = D3DXVECTOR2(0, 1);

	//flag位置取得
	D3DXVECTOR3 flagPos = ((PlayScene*)pParent_)->GetStage()->GetFlag()->GetPosition();

	//フラッグと現在地のベクトル作成
	D3DXVECTOR2 vecToFlag2D = D3DXVECTOR2(flagPos.x - position_.x, flagPos.z - position_.z);

	//Y軸の回転量求める
	float radY;
	radY = acos(D3DXVec2Dot(&baseXY, &vecToFlag2D) / (D3DXVec2Length(&baseXY) * D3DXVec2Length(&vecToFlag2D)));


	//STARTPOSのxが負ならそのまま正ならマイナスにする
	if (position_.x > 0)
	{
		rotate_.y = -D3DXToDegree(radY);
	}
	else
	{
		rotate_.y = D3DXToDegree(radY);
	}
}

//更新
void Target::Update()
{
	position_ = ((PlayScene*)pParent_)->GetBall()->GetPosition();

	CameraMove();

	Rotate();

	D3DXVECTOR3 a = zoomVec;
	int b = 0;

	//球が動き終わったらカメラ戻す
	if (((PlayScene*)pParent_)->GetBall()->IsMoveBefore() == true && ((PlayScene*)pParent_)->GetBall()->IsMoveCurrent() == false)
	{
		zoomVec = D3DXVECTOR3(0, 0, 0);
		pCamera_->SetPosition(DEF_CAM_POS);
	}
}

//描画
void Target::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}


//開放
void Target::Release()
{
}

//カメラの移動
void Target::CameraMove()
{
	if (Input::IsKeyDown(DIK_F))
	{
		pCamera_->SetPosition(TOP_CAM_POS);
	}
	if (Input::IsKeyUp(DIK_F))
	{
		pCamera_->SetPosition(DEF_CAM_POS);
	}
}

//左右への回転
void Target::Rotate()
{
	if (Input::IsKey(DIK_RIGHT))
	{
		rotate_.y += 0.5f;
	}
	if (Input::IsKey(DIK_LEFT))
	{
		rotate_.y -= 0.5f;
	}
}

//注視点に向けてズーム
void Target::Zoom()
{
	D3DXVECTOR3 vec =tagPos - camPos;

	zoomVec += vec / ZOOM_SPEED;

	pCamera_->SetPosition(DEF_CAM_POS + zoomVec);
}