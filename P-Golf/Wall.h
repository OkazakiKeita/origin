#pragma once
#include <string>
#include <vector>
#include "Engine/IGameObject.h" 

//壁を管理するクラス
class Wall : public IGameObject
{
	int hModel_;
	int hModel_in_;

	//壁のサイズ
	D3DXVECTOR3 wallSize_[30];
public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//デストラクタ
	~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//コライダーの位置登録
	void SettingColiderPos();

	//コンマ区切りでデータ分割
	std::vector<std::string> GetComma(std::string& input, char comma);
};