#pragma once
#include "Engine/IGameObject.h"

//パワーバーを管理するクラス
class Power : public IGameObject
{
private:
	int hPictPower_;
	int hpictBase_;
	//打力
	int pow_;
public:
	//コンストラクタ
	Power(IGameObject* parent);

	//デストラクタ
	~Power();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetPower(int power)
	{
		pow_ = power;
	}
};