#pragma once
#include "Engine/IGameObject.h" 
#include "Engine/Camera.h"


//注視点を管理するクラス
class Target : public IGameObject
{
private:
	int hModel_;

	Camera* pCamera_;

	//カメラズーム用ベクトル
	D3DXVECTOR3 zoomVec;
	//カメラポジション
	D3DXVECTOR3 camPos;
	//注視点
	D3DXVECTOR3 tagPos;

public:
	//コンストラクタ
	Target(IGameObject* parent);

	//デストラクタ
	~Target();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//カメラの場所変更
	void CameraMove();

	//回転
	void Rotate();

	void Zoom();
};