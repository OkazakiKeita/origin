#pragma once
#include "Engine/IGameObject.h"

#define MAXCOUNT 9

//パットカウントを管理するクラス
class PatCount : public IGameObject
{
	int hPictPar_;
	int hPictCount_;
	int hPictPatCount_[9];
	//打数
	int patCnt_;
public:
	//コンストラクタ
	PatCount(IGameObject* parent);

	//デストラクタ
	~PatCount();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//打数の登録
	void SetPatCnt(int count)
	{
		patCnt_ = count;
	}
};