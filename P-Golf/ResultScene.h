#pragma once
#include "Engine/global.h"

#define MAX_RESULT 5


//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
private:
	int patCnt_;
	int hPict_;

	std::string ResultPict[MAX_RESULT] = {
	"HoleInOne",
	"Eagle",
	"Birdy",
	"Par",
	"Over"
	};
public:
	//コンストラクタ
	//引数：parent
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetPatCnt(int patcnt)
	{
		patCnt_ = patcnt;
	}
};