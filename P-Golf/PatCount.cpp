#include "PatCount.h"
#include"Engine/Image.h"

//コンストラクタ
PatCount::PatCount(IGameObject * parent)
	:IGameObject(parent, "PatCount"), hPictPar_(-1)
{
}

//デストラクタ
PatCount::~PatCount()
{
}

//初期化
void PatCount::Initialize()
{
	std::string CountfileName[MAXCOUNT]
	{
		"Count1",
		"Count2",
		"Count3",
		"Count4",
		"Count5",
		"Count6",
		"Count7",
		"Count8",
		"Count9",
	};
	for (int i = 0; i < MAXCOUNT; i++)
	{
		hPictPatCount_[i] = Image::Load("Data/Image/" + CountfileName[i] + ".png");
	}

	hPictPar_ = Image::Load("Data/Image/PARCount.png");
	hPictCount_ = Image::Load("Data/Image/PatCount.png");
}

//更新
void PatCount::Update()
{
}

//描画
void PatCount::Draw()
{
	Image::Draw(hPictPar_);
	Image::Draw(hPictCount_);

	Image::Draw(hPictPatCount_[patCnt_]);
}

//開放
void PatCount::Release()
{
}