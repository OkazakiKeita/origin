#include "Stage1.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "Flag.h"

//コンストラクタ
Stage1::Stage1(IGameObject * parent)
	:IGameObject(parent, "Stage1"), hModel_(-1)
{
}

//デストラクタ
Stage1::~Stage1()
{
}

//初期化
void Stage1::Initialize()
{
	hModel_ = Model::Load("Data/Model/Stage1.Fbx");
	assert(hModel_ >= 0);
	pFlag_ = CreateGameObject<Flag>(this);
	pWall_ = CreateGameObject<Wall>(this);
	pTree_ = CreateGameObject<Tree>(this);
}

//更新
void Stage1::Update()
{
}

//描画
void Stage1::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}


//開放
void Stage1::Release()
{
}