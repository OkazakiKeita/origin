#include "Ball.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "Stage1.h"
#include "PlayScene.h"

const float BALL_SIZE = 0.427f;
const float SLOPE_DEC = 0.004f;
const float PI = 3.14f;
const float CIRCUMFERENCE = 2 * PI * BALL_SIZE;
const int MAX_PAT_COUNT = 9;


const D3DXVECTOR3 STARTPOS[4] = {D3DXVECTOR3(80, 0, 80), D3DXVECTOR3(80, 0, -80), D3DXVECTOR3(-80, 0, 80), D3DXVECTOR3(-80, 0, -80)};

//コンストラクタ
Ball::Ball(IGameObject * parent)
	:IGameObject(parent, "Ball"), hModel_(-1), power_(0),
	currentMoveFlg_(false),beforeMoveFlg_(false), pat_(0), passedFlame_(0),
	m(46.0f), g(9.8f), n(m*g), u(0.2f), shoot_(false), move_(D3DXVECTOR3(0, 0, 0)), yPos(0)
{
}

//デストラクタ
Ball::~Ball()
{
}

//初期化
void Ball::Initialize()
{
	hModel_ = Model::Load("Data/Model/Ball.Fbx");
	assert(hModel_ >= 0);
	SetSphereCollider(D3DXVECTOR3(0, BALL_SIZE, 0), BALL_SIZE);

	pPatCnt_ = CreateGameObject<PatCount>(this);
	pPatCnt_->SetPatCnt(pat_);

	pPower_ = CreateGameObject<Power>(this);

	srand((unsigned int)time(NULL));
	int index = rand() % 4;

	position_.x = STARTPOS[index].x;
	position_.z = STARTPOS[index].z;


	FollowGround();

}

//更新
void Ball::Update()
{
	//規定打数超えたら強制的に終了
	if (pat_ >= MAX_PAT_COUNT)
	{
		SceneManager::ChangeScene(SCENE_ID_RESULT, pat_);
	}

	//球が動いていないなら打球可能
	if (!currentMoveFlg_)
	{
		//打球処理
		Shot();
	}
	else
	{
		//移動処理
		Move();
		Rotate();
	}

	beforeMoveFlg_ = currentMoveFlg_;

	SendPower();

	//停止処理;
	if (currentMoveFlg_)
	{
		MoveStop();
	}

	//経過フレームの観察
	if (currentMoveFlg_)
	{
		passedFlame_++;
	}
}

//描画
void Ball::Draw()
{
	Model::SetMatrix(hModel_, localMatrix_);
	Model::Draw(hModel_);
}


//開放
void Ball::Release()
{
}


void Ball::OnCollision(IGameObject* ptarget, int num)
{
	//壁に反射させる
	if (ptarget->GetObjectName() == "Wall")
	{
		move_ = Reflect(ptarget, num);
	}
	if (ptarget->GetObjectName() == "Flag")
	{
		SceneManager::ChangeScene(SCENE_ID_RESULT, pat_);
	}
}

D3DXVECTOR3 Ball::Reflect(IGameObject* ptarget, int num){
	//法線の情報とる
	D3DXVECTOR3 normal = ptarget->GetNormal(num);
	
	//移動ベクトルとる
	D3DXVECTOR3 moveVec = move_;

	//反射ベクトルを作る
	D3DXVECTOR3 ReflectVec = moveVec + ((2 * D3DXVec3Dot(&(-moveVec), &normal)) * normal);
	
	return ReflectVec;
}


//打球の強さ設定
void Ball::Shot()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		power_ = 0;
	}
	if (Input::IsKey(DIK_SPACE))
	{
		if (power_ < 1)
		{
			power_ += 0.01f;
		}
		if (power_ > 1)
		{
			power_ = 1;
		}
	}
	if (Input::IsKeyUp(DIK_SPACE))
	{
		passedFlame_ = 0;
		
		D3DXVECTOR3 move(0, 0, power_);
		D3DXMATRIX matY;
		float rotY = ((PlayScene*)pParent_)->GetTarget()->GetRotate().y;
		D3DXMatrixRotationY(&matY, D3DXToRadian(rotY));

		D3DXVec3TransformCoord(&move_, &move, &matY);


 		shoot_ = true;

		currentMoveFlg_ = true;
	}
}

//玉の停止処理
void Ball::MoveStop()
{
	//移動方向のベクトルの長さ
	float moveLen = D3DXVec3Length(&move_);
	
	//減速しているベクトルの長さ
	float decLen = D3DXVec3Length(&(move_ * (0.2f * (passedFlame_ / 60.0f))));

	if (moveLen - decLen < moveLen / 2)
	{
		((PlayScene*)pParent_)->GetTarget()->Zoom();
	}

	
	//長さが0を下回ったら止めて、打球可能にする
	if (moveLen - decLen < 0)
	{
		move_.x = move_.z = 0;
		
		currentMoveFlg_ = false;
		
		pPatCnt_->SetPatCnt(++pat_);
		shoot_ = false;

		currentMoveFlg_ = false;
		power_ = 0;
	}
}

//地面に添わせる処理
void Ball::FollowGround()
{
	RayCastData raydata;
	raydata.startPos = position_;
	raydata.startPos.y = -BALL_SIZE;
	raydata.rayDir = D3DXVECTOR3(0, -1, 0);

	int hStageModel = ((PlayScene*)pParent_)->GetStage()->GetStageHandle();

	Model::RayCast(hStageModel, raydata);
	if (raydata.hit)
	{
		//position_.y = -raydata.dist;
		///////////////////////////////////
		if (position_.y == 0)
		{
			position_.y = -raydata.dist;
			gNor_ = raydata.normal;
		}
		yPos = -raydata.dist;
		///////////////////////////////////
	}
	//法線情報が変化したとき再登録
	if (groundNormal_ != raydata.normal)
	{
		groundNormal_ = raydata.normal;
	}
}



void Ball::Move()
{
	nowPos_ = position_;
	nowLength_ = move_ + (-move_) * (0.2f * (passedFlame_ / 60.0f));
		
	//球のポジション変更
	position_ += nowLength_;
	
	//地面に沿わせる
	FollowGround();


	//浮かせる
	if (position_.y > yPos)
	{
		position_.y -= 0.2f;
	}
	if (position_.y < yPos)
	{
		position_.y = yPos;
	}

	//斜面時の加減速
	D3DXVECTOR3 groundNor;
	groundNor = groundNormal_;


	float norX = groundNor.x;
	float norZ = groundNor.z;

	//斜面時の計算
	if (currentMoveFlg_)
	{
		move_.x += norX * SLOPE_DEC;
		move_.z += norZ * SLOPE_DEC;
	}
}


void Ball::SendPower()
{
	//ゲージの管理
	pPower_->SetPower((int)(power_ / 0.01));
}

void Ball::Rotate()
{
	//基本ベクトル(X,Z)方向
	D3DXVECTOR2 baseXZ = D3DXVECTOR2(0, 1);

	D3DXVECTOR2 mVec2;

	D3DXVECTOR3 translation = position_ - nowPos_;

	mVec2.x = translation.x;
	mVec2.y = translation.z;

	//進行方向の二次元ベクトルと(0,1)の角度を求める
	float radY = acos(D3DXVec2Dot(&baseXZ, &mVec2) / (D3DXVec2Length(&baseXZ) * D3DXVec2Length(&mVec2)));

	//その角度分rotate_.yを回転させる
	if (mVec2.x < 0)
	{
		rotate_.y = D3DXToDegree(radY - 2 * radY);
	}
	else
	{
		rotate_.y = D3DXToDegree(radY);
	}

	//速度に応じて回転量を設定する
	rotate_.x += D3DXVec3Length(&translation) * 30;
}