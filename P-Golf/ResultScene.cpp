#include "ResultScene.h"
#include "Engine/Image.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), hPict_(-1)
{
}

//初期化
void ResultScene::Initialize()
{
}

//更新
void ResultScene::Update()
{
	if (hPict_ == -1)
	{
		//打数により変化
		if (patCnt_ >= MAX_RESULT)
		{
			hPict_ = Image::Load("Data/Image/" + ResultPict[MAX_RESULT - 1] + ".png");
		}
		else
		{
			hPict_ = Image::Load("Data/Image/" + ResultPict[patCnt_] + ".png");
		}
	}

	//プレイシーンに戻る
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}