#pragma once
#include "Engine/IGameObject.h" 
#include "PatCount.h"
#include "Power.h"

//球を管理するクラス
class Ball : public IGameObject
{
	//モデル番号
	int hModel_;

	//通常移動のベクトル
	D3DXVECTOR3 move_;

	//斜面方向の運動
	float moveSlopeX_;
	float moveSlopeY_;
	float moveSlopeZ_;

	//打つ時のパワー
	float power_;

	//現在の移動距離
	D3DXVECTOR3 nowPos_;
	D3DXVECTOR3 nowLength_;

	//今動いているか、1フレーム前に動いているか
	bool beforeMoveFlg_;
	bool currentMoveFlg_;

	//パットカウントクラス
	PatCount* pPatCnt_;
	//パワークラス
	Power* pPower_;

	//打数
	int pat_;

	//経過フレーム数
	int passedFlame_;

	//地面の法線
	D3DXVECTOR3 groundNormal_;

	//質量
	float m;
	//重力加速度
	float g;
	//垂直抗力
	float n;
	//動摩擦係数
	float u;
	//最大静止摩擦力
	float friction;

	//打球可能か
	bool shoot_;

	float yPos;

	//地面の法線
	D3DXVECTOR3 gNor_;

public:
	//コンストラクタ
	Ball(IGameObject* parent);

	//デストラクタ
	~Ball();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* ptarget, int num);

	D3DXVECTOR3 Reflect(IGameObject * ptarget, int num);

	//打球処理
	void Shot();

	//停止処理
	void MoveStop();

	//地面に沿わせる
	void FollowGround();

	//移動処理
	void Move();

	//パワーをPowerに送るための処理
	void SendPower();

	void Rotate();

	//今のフレーム動いてるか
	bool IsMoveCurrent()
	{
		return currentMoveFlg_;
	}

	//前のフレーム動いてるか
	bool IsMoveBefore()
	{
		return beforeMoveFlg_;
	}

	//打数取得
	int GetPatCnt()
	{
		return pat_;
	}
};
