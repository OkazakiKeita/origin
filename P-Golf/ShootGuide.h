#pragma once
#include "Engine/IGameObject.h"

//ガイドを管理するクラス
class ShootGuide : public IGameObject
{
private:
	int hModel_;
	//前後どちらに動くか
	bool moveFront_;
	//移動するか
	bool canMove_;
public:
	//コンストラクタ
	ShootGuide(IGameObject* parent);

	//デストラクタ
	~ShootGuide();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ガイドを前後させる
	//引数: ボールが動いているかどうか
	void GuideMove(bool canMove);

	//ガイドを動かすかどうかセット
	void SetCanMove(bool can)
	{
		canMove_ = can;
	}
};