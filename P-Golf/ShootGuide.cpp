#include "ShootGuide.h"
#include "Engine/Model.h"

//コンストラクタ
ShootGuide::ShootGuide(IGameObject * parent)
	:IGameObject(parent, "ShootGuide"), canMove_(true), moveFront_(true)
{
}

//デストラクタ
ShootGuide::~ShootGuide()
{
}

//初期化
void ShootGuide::Initialize()
{
	hModel_ = Model::Load("Data/Model/ShootGuide.fbx");
}

//更新
void ShootGuide::Update()
{
	GuideMove(canMove_);
}

//描画
void ShootGuide::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void ShootGuide::Release()
{
}

//ガイドを動かす
void ShootGuide::GuideMove(bool canMove)
{
	if (canMove)
	{
		if (moveFront_)
		{
			position_.z += 0.1f;
		}
		else
		{
			position_.z -= 0.1f;
		}

		if (position_.z > 4)
		{
			moveFront_ = false;
		}
		if (position_.z < 0)
		{
			moveFront_ = true;
		}
	}
	else
	{
		position_.z = 0;
	}
}
