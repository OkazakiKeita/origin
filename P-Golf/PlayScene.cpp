#include "PlayScene.h"
#include "Stage1.h"
#include "Wall.h"

//#include "Power.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")	//IGameObjectのコンストラクタを呼び出している	第一親の名前　第二シーンの名前
{
}

//初期化
void PlayScene::Initialize()
{
	//ステージの生成
	pStage = CreateGameObject<Stage1>(this);
	//ボール生成
	pBall = CreateGameObject<Ball>(this);
	//注視点位置用
	pTarget = CreateGameObject<Target>(this);
	//プレイヤー生成
	pPlayer = CreateGameObject<Player>(this);
}

//更新
void PlayScene::Update()
{
	//球が止まったとき
	if (pBall->IsMoveCurrent() == false && pBall->IsMoveBefore() == true)
	{
		//プレイヤー移動
		pPlayer = CreateGameObject<Player>(this);
		pPlayer->SetPosition(pBall->GetPosition());
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}