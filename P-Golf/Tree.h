#pragma once
#include "Engine/IGameObject.h"

//ステージを管理するクラス
class Tree : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	Tree(IGameObject* parent);

	//デストラクタ
	~Tree();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};