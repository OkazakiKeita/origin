#include "Player.h"
#include "Engine/Model.h"
#include "Ball.h"
#include "PlayScene.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hModel_(-1), canRotate_(false), nextPat_(false)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	pShootGuide_ = CreateGameObject<ShootGuide>(this);
	hModel_ = Model::Load("Data/Model/Player.Fbx");
	assert(hModel_ >= 0);

	position_ = ((PlayScene*)pParent_)->GetBall()->GetPosition();
}

//更新
void Player::Update()
{
	canRotate_ = ((PlayScene*)pParent_)->GetBall()->IsMoveCurrent();

	//ボールが動いていないならプレイヤーをY軸で回転させる
	if (canRotate_ == false)
	{
		rotate_ = ((PlayScene*)pParent_)->GetTarget()->GetRotate();
	}
	else
	{
		pShootGuide_->SetCanMove(false);
	}

	//ひとつ前のフレームが止まっているかの確認
	nextPat_ = ((PlayScene*)pParent_)->GetBall()->IsMoveBefore();

	if (canRotate_ == false && nextPat_ == true)
	{
		KillMe();
	}
}

//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}




//開放
void Player::Release()
{
}