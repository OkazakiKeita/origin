#include "Tree.h"
#include "Engine/Model.h"
#include "Stage1.h"

//コンストラクタ
Tree::Tree(IGameObject * parent)
	:IGameObject(parent, "Tree")
{
}

//デストラクタ
Tree::~Tree()
{
}

//初期化
void Tree::Initialize()
{
	hModel_ = Model::Load("Data/Model/Tree.Fbx");
}

//更新
void Tree::Update()
{
}

//描画
void Tree::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}


//開放
void Tree::Release()
{
}