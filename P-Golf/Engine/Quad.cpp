#include "Quad.h"
#include "Direct3D.h"
#include "Global.h"



Quad::Quad():
	pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pTexture_(nullptr), material_({0})		//ポインタの初期化,配列の初期化
{
}


Quad::~Quad()
{
	SAFE_RELEASSE(pTexture_);
	SAFE_RELEASSE(pIndexBuffer_);	//解放
	SAFE_RELEASSE(pVertexBuffer_);//解放	作った順と逆順に開放
}

void Quad::Load(const char* FileName)
{
	//頂点情報をまとめた構造体の配列
	Vertex vertexList[] = {
	   D3DXVECTOR3(-1, 1, 0), D3DXVECTOR3(0,0,-1),  D3DXVECTOR2(0,0),			//vertexList[0]、左上
	   D3DXVECTOR3(1, 1, 0),  D3DXVECTOR3(0,0,-1),	D3DXVECTOR2(1,0),			//vertexList[1]、右上
	   D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0,0,-1),  D3DXVECTOR2(1,1),			//vertexList[2]、右下
	   D3DXVECTOR3(-1, -1, 0),D3DXVECTOR3(0,0,-1),  D3DXVECTOR2(0,1),			//vertexList[3]、左下
	};

	//頂点バッファの作成
	Direct3D::pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);			//D3DFVF_XYZ、位置情報	D3DFVF_DIFUSE 拡散反射光（色）情報
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);		//バッファの内容をロックする
	memcpy(vCopy, vertexList, sizeof(vertexList));		//vCopyにvertexListをサイズ分コピーする
	pVertexBuffer_->Unlock();							//バッファのロックを外す


	//インデックス情報（どの順番でつなぐか）
	int indexList[] = { 0, 2, 3, 0, 1, 2 };		//0,2,3で三角、0,1,2で三角を作る	時計回りになるように書く

	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);

	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);		//バッファロック
	memcpy(iCopy, indexList, sizeof(indexList));		//メモリコピー
	pIndexBuffer_->Unlock();							//ロック解除

	
	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, FileName,				//使える画像サイズは2の乗数にしておかないと変な表示になる。幅は1024にしとくといいかも
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);			//最後にテクスチャのアドレス
	assert(pTexture_ != nullptr);

	//マテリアルの作成
	material_.Diffuse.r = 1.0f;				//何パーセントその色を返すか(0〜1の間で設定)
	material_.Diffuse.g = 1.0f;				//同上
	material_.Diffuse.b = 1.0f;				//同上
}

void Quad::Draw(const D3DXMATRIX &matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);		//変形の適用	ワールド行列（位置や回転、変形などの情報を持っている） 	
	Direct3D::pDevice->SetTexture(0, pTexture_);				//使用するテクスチャの設定	第一テクスチャ番号　第二使用するテクスチャ
	Direct3D::pDevice->SetMaterial(&material_);					//使用するマテリアルの設定

	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));	//表示したいデータが入ってる場所を指定する
	Direct3D::pDevice->SetIndices(pIndexBuffer_);

	Direct3D::pDevice->SetFVF(D3DFVF_XYZ |D3DFVF_NORMAL | D3DFVF_TEX1/*D3DFVF_DIFFUSE*/);			//ビット演算で位置と情報をまとめる

	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);	//第一使い方	第四頂点データの数	第六出したいポリゴンの数
}
