#pragma once
#include "Global.h"

const D3DXVECTOR3 vertexIndex[]{
	D3DXVECTOR3(0.5, 0.5, 0.5) , D3DXVECTOR3(0.5, 0.5, -0.5) , D3DXVECTOR3(0.5, -0.5, 0.5) , D3DXVECTOR3(0.5, -0.5, -0.5),
	D3DXVECTOR3(-0.5, 0.5, 0.5), D3DXVECTOR3(-0.5, 0.5, -0.5), D3DXVECTOR3(-0.5, -0.5, 0.5), D3DXVECTOR3(-0.5, -0.5, -0.5)
};
class Collider
{
	//プレイヤーの原点から見た位置
	D3DXVECTOR3 center_;
	//半径
	float		radius_;
	//Colliderの所持者を管理
	IGameObject* owner_;
	//中心からどれだけの長さか
	D3DXVECTOR3 size_;

	bool IsBoxCollider_;

	const D3DXVECTOR3 vertexIndex_[8];

public:
	//コンストラクタ
	//引数 第一:IGameObject*型 owner, 第二:D3DXVECTOR3型 center, 第三:float型 radius
	Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	Collider(IGameObject* owner, D3DXVECTOR3 center, D3DXVECTOR3 size);
	//デストラクタ
	~Collider();

	//当たったか判定する
	//引数 Collider型 当たった相手
	//戻り値 bool型
	bool IsHit(Collider& target, int num);

	bool IsHitBallBox(Collider& target, int num);

	bool IsHitBallBall(Collider& target, int num);

	bool IsHitBoxBox(Collider& target, int num);
	bool IsInRadius(D3DXVECTOR3 spherepos, D3DXVECTOR3 boxpos, D3DXVECTOR3 boxsize, float radius);
};

