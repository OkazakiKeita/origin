#include <assert.h>
#include "Input.h"

namespace Input		//初期化Direct3Dのと違うけど似てる
{
	//クラスではないので直接初期化
	LPDIRECTINPUT8   pDInput = nullptr;
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;

	//BYTE型はunsignedchar型（1バイト）
	BYTE keyState[256] = { 0 };
	BYTE prevKeyState[256] = { 0 };    //前フレームでの各キーの状態


	void Initialize(HWND hWnd)
	{
		//本体を作成する
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		//デバイスの生成
		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		assert(pKeyDevice != nullptr);
		//デバイスの種類を指定（キーボード）
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
		//強調レベルの設定
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	}

	void Update()
	{
		//キーの現在の情報を取得
		memcpy(prevKeyState, keyState, sizeof(keyState));

		//キーボードを見失ったときに探してくれる
		pKeyDevice->Acquire();
		//すべてのキーの情報を取得する
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);
	}

	bool IsKey(int keyCode)
	{
		//0x80との&をすると8ビット目の値が1かどうかを確認できる
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、さらに前回は押してない
		//条件が二つ必要
		if (IsKey(keyCode) &&
			(prevKeyState[keyCode] & 0x80) == 0)
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//今は押してなくて、さらに前回は押してる
		if (IsKey(keyCode) == false && (prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	void Release()
	{
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}
}