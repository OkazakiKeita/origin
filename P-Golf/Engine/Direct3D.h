#pragma once
#include"Global.h"

namespace Direct3D		//Direct3Dの関数名だと分かるようになる Direct3D::関数名でそれにアクセスできる
{
	extern LPDIRECT3D9	pD3d;	   //Direct3Dオブジェクト		LPで始まるのはポインタ DIRECT3Dを扱うクラス	SPriteでヘッダ使うときにもう一度つくってしまうからこれで参照するようにする
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト　ディスプレイと直にやり取りする				※重要

	//初期化
	//引数 hwnd	ウィンドウハンドル
	//戻値 なし
	void Initialise(HWND hWnd);

	//描画を開始させる
	//引数	なし
	//戻値	なし
	void BeginDraw();

	//描画を終了させる
	//引数	なし
	//戻値	なし
	void EndDraw();

	//開放
	//引数	なし
	//戻値	なし
	void Release();
}