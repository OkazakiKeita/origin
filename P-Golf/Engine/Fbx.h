#pragma once
#include <fbxsdk.h>
#include "Global.h"

#pragma comment(lib,"libfbxsdk-mt.lib")		//どこかに書いておく

struct RayCastData
{
	//レイの飛ばす位置
	D3DXVECTOR3 startPos;
	//レイの飛ばす方向
	D3DXVECTOR3 rayDir;
	//法線情報
	D3DXVECTOR3 normal;
	D3DXVECTOR3 lowerPos;
	float		dist;
	bool		hit;
};

class Fbx
{
	struct Vertex					//D3DFVF系の宣言を確認してその順番に作る
	{
		D3DXVECTOR3 pos;			//位置情報
		D3DXVECTOR3 normal;			//法線情報
		D3DXVECTOR2 uv;				//uv情報（二次元情報）
	};

	FbxManager*  pManager_;			//指示する
	FbxImporter* pImporter_;		//インポート（ロード）
	FbxScene*    pScene_;			//読み込んだシーンを保存する


	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;		//ポインタの配列

	LPDIRECT3DTEXTURE9* pTexture_;	//テクスチャ
	D3DMATERIAL9* pmaterial_;		//マテリアル

	int vertexCount_;				//頂点の個数を記録
	int polygonCount_;				//ポリゴンの個数を記録
	int indexCount_;				//インデックスの個数を記録
	int materialCount_;				
	int* polygonCountOfMaterial_;

	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx();
	~Fbx();

	void Load(const char* FileName);
	void Draw(const D3DXMATRIX &matrix);
	void RayCast(RayCastData& data, const D3DXMATRIX &matrix);
};