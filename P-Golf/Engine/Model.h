#pragma once
#include<string>
#include"Fbx.h"
#include"Direct3D.h"

namespace Model
{
	//modldataの構造体
	struct ModelData
	{
		std::string fileName;	//文字列(ファイル名を入れる)
		Fbx* pFbx;				//FBXの情報
		D3DXMATRIX matrix;		//行列

		//c++だと構造体でもコンストラクタが使える
		//コンストラクタ
		ModelData() :fileName(""), pFbx(nullptr)
		{
			//行列の初期化は中に書かないといけない
			//単位行列として初期化(特に何もしない)
			D3DXMatrixIdentity(&matrix);
		}
	};

	//メモリに引数で指定したfailenameのfbx情報をロードする
	//すでにある場合はそのアドレスをコピーして扱う
	//引数 string型 filename
	//戻り値 int型 datalistの何番目に入っているか
	int Load(std::string fileName);

	//引数番目のfbxを描画する
	//引数 int型 モデル番号
	//戻り値 void型
	void Draw(int handle);

	//第一引数番目のモデルのmatrixに第二引数のmatrixをセットする
	//第一引数: int型 モデル番号
	//第二引数: D3DXMATRIX型 行列
	//戻り値 void
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//解放処理を行う関数
	//引数 int型 モデル番号
	//戻り値 void型
	void Release(int handle);

	//すべて解放する
	//引数 なし
	//戻り値 void型
	void AllRelease();

	void RayCast(int handle, RayCastData & data);
};