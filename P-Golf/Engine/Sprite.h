#pragma once
#include"Global.h"
class Sprite
{
	LPD3DXSPRITE         pSprite_;  //スプライト	
	LPDIRECT3DTEXTURE9 pTexture_; //テクスチャ
	
public:
	Sprite();
	~Sprite();

	//スプライトとテクスチャの作成
	//引数	char*型
	//戻値	void型
	void Load(const char *FileName);		//変化しないのでconst

	//行列の変形、画面の描画
	//引数	D3DXMATRIX型
	//戻値	void型
	void Draw(const D3DXMATRIX& matrix, RECT* rect, float alpha);		//参照渡し	&を引数につける

	D3DXVECTOR2 GetTextureSize();
};

