#pragma once
#include <xact3.h>
#include <string>

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}

namespace Audio
{
		
	void Initialize();

	void LoadWaveBank(std::string lpFileName);

	void LoadSoundBank(std::string lpFileName);

	void Release();

	void Play(std::string szFriendlyname);

	void Stop(std::string szFriendlyname);
};