#include "Direct3D.h"
#include "Global.h"

LPDIRECT3D9	Direct3D::pD3d = nullptr;						//externがあるので初期化
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;			//externがあるので初期化


void Direct3D::Initialise(HWND hWnd)		//クラスじゃないけど書き方は同じ
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);		//LP何ちゃらの型はポインタなのでnewを使えない　たいていCreateがあるのでそれを使う
	assert(pD3d != nullptr);
													//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;							//TRUE ウィンドウの中に表示　FALSE 全画面
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;			//幅
	d3dpp.BackBufferHeight = g.screenHeight;			//高さ
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,	//Createする
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);


	//アルファブレンド（半透明表示の設定）
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);					//ここにもエラー確認は入れたほうがいいがやりすぎかも
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);			//第二がTRUEでライトを使用する設定になる	書かなくてもTRUEになる

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));	//指定された場所からサイズ(第二引数)分0で初期化する
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//平行光源
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//光の向き（進行方向）	(x,y,z)
	lightState.Diffuse.r = 1.0f;					//ライトの色、0〜1の間で表現（R)
	lightState.Diffuse.g = 1.0f;					//（G）	計算が楽だから0〜1の範囲
	lightState.Diffuse.b = 1.0f;					//（B）
	pDevice->SetLight(0, &lightState);				//第一ライトの番号、第二使用するライト
	pDevice->LightEnable(0, TRUE);					//第一ライトの番号、第二TRUEでライトON

	//カメラ
	D3DXMATRIX view, proj;
	//ビュー行列を作るための関数 第一:入れるとこ　第二:カメラの位置(視点の位置) 第三:焦点,注視点(どこを見るか) 第四:カメラの上方向ベクトル(カメラの上がどっちか)(基本は010)
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 10, -10), &D3DXVECTOR3(0, 0, 10), &D3DXVECTOR3(0, 1, 0));
	//第一:どの行列として使うか	第二:行列
	pDevice->SetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列を作るための関数 第一:入れるとこ 第二:視野角,画角(縦) 第三:アスペクト比(横/高さ) 第四:カメラからどれくらいの距離で描画するか(近クリッピング面)
	//第五:カメラからどれくらいの距離まで描画するか(遠クリッピング面)
	//Perspective（遠近感）
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 1000.0f);
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);

}

void Direct3D::BeginDraw()
{
	//画面をクリア
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(194, 243, 252), 1.0f, 0);	//毎フレーム画面をまっさらにする (色変えられるよ。黒白以外できつくない色に設定するように)

																									//描画開始
	pDevice->BeginScene();	//はじめる時にBeginScene
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();	//終わるときにEndScene

							//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);	//フロントバッファとバックバッファを入れ替えることで、ちらつきを低減させる
}

void Direct3D::Release()
{
	//開放処理
	SAFE_RELEASSE(pDevice);			//後で作ったから先に開放
	SAFE_RELEASSE(pD3d);			//先に作ったから後で開放
}
