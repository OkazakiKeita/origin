#pragma once
#include "IGameObject.h"


enum SCENE_ID
{
	SCENE_ID_TITLE,
	SCENE_ID_EXPLANATION,
	SCENE_ID_PLAY,
	SCENE_ID_RESULT
};





//シーンを管理するクラス
class SceneManager : public IGameObject
{
	//現在のシーン
	static SCENE_ID currentSceneID_;
	//次のシーン
	static SCENE_ID nextSceneID_;
	//現在のシーンのアドレス
	static IGameObject* pCurrentScene;

	static int patCnt_;

public:
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//nextSceneに引数を登録する
	//引数 enumのSCENE_ID 次のシーン
	//戻り値 void型
	static void ChangeScene(SCENE_ID next, int patcnt = -1);

	//staticなメンバを返すので関数もstaticにする
	//現在のシーンを獲得する
	//引数 なし
	//戻り値 IGameObject*型 pcurrentscene
	static IGameObject* GetCurrentScene() {
		return pCurrentScene;
	}
};