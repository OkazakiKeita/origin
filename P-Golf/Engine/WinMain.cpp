#include <Windows.h>		//これでWinAPIが使える
#include"Global.h"
#include"Direct3D.h"
#include "RootJob.h"
#include"Model.h"
#include"Image.h"

#include "Audio.h"

//debugモードだったらやる
#ifdef _DEBUG
#include <crtdbg.h>
#endif


//リンカ		どっかに書いてあればいい
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")


//定数
const char* WIN_CLASS_NAME = "SampleGame";	//クラスの作成とウィンドウの作成で使うため	
const int	WINDOW_WIDTH = 800;				//ウィンドウの幅		マジックナンバー削除のため
const int	WINDOW_HEIGHT = 800;			//ウィンドウの高さ	

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//グローバル変数
Global g;		//グローバル型の構造体
RootJob* pRootJob;



//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;			//設定内容の場所
	wc.cbSize = sizeof(WNDCLASSEX);							//この構造体のサイズ	SIzeを指定しないといけない　（〜Sizeがきたらsizeofで設定）
	wc.hInstance = hInstance;								//インスタンスハンドル	上で割り振られた番号を登録
	wc.lpszClassName = WIN_CLASS_NAME;	        			//ウィンドウクラス名	作ろうとしている設計図の名前を入れる
	wc.lpfnWndProc = WndProc;								//ウィンドウプロシージャ	下にあるウィンドウプロシージャの名前をそのまま書かないといけない	ここで書くためにプロトタイプ宣言
	wc.style = CS_VREDRAW | CS_HREDRAW;						//スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);				//アイコン	下に出る
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);				//小さいアイコン　ウィンドウの左上（タイトルの隣に出る）
	wc.hCursor = LoadCursor(NULL,IDC_ARROW);				//マウスカーソル	基本は白黒でドット　カラーのを使いたいときはカーソル非表示にしてその上に絵を表示する形になる
	wc.lpszMenuName = NULL;									//メニュー（なし）	メニューを作るときに使う
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）基本は変えない
	RegisterClassEx(&wc);				//クラスを登録（設定内容の登録）

	RECT winRect = { 0,0,WINDOW_WIDTH, WINDOW_HEIGHT };		//left,top,right.bottom	サイズを決める				※重要
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);	//クライアント領域を上のサイズに合わせる(アドレス、スタイル、FALSE)		※重要



	//ウィンドウを作成		HWND->ウィンドウハンドルを扱う型	重要	
	HWND hWnd = CreateWindow(			//作られたウィンドウの番号が入る
		WIN_CLASS_NAME,					//ウィンドウクラス名	上で作った設計図と同じ名前を入れる
		"P-Golf",    			//タイトルバーに表示する内容	左上にかかれるやつ
		WS_OVERLAPPEDWINDOW,			//スタイル（普通のウィンドウ）ウィンドウの種類が変えられる
		CW_USEDEFAULT,					//表示位置左（おまかせ）
		CW_USEDEFAULT,					//表示位置上（おまかせ)
		winRect.right - winRect.left,	//ウィンドウ幅
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		NULL,							//親ウインドウ（なし)	大きいウィンドウを親に設定することが多い
		NULL,							//メニュー（なし）
		hInstance,						//インスタンス
		NULL							//パラメータ（なし）
	);
	assert(hWnd != NULL);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	//externされているグローバル変数に値を代入
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの初期化(準備)
	Direct3D::Initialise(hWnd);

	//DirectInputの初期化
	Input::Initialize(hWnd);

	//RootJobの初期化
	pRootJob = new RootJob;
	assert(pRootJob != nullptr);
	pRootJob->Initialize();


	

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));	//一気に初期化する　どこからどの分
	while (msg.message != WM_QUIT)	//抜けたら終わる
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))		//Peekはメッセージ受け取ったとき　例＞終了のときは優先されるから上にきてる
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

			//入力情報の更新
			Input::Update();
			//入力を受け取って処理をするのでInputの後に書く
			pRootJob->UpdateSub();
			//描画開始
			Direct3D::BeginDraw();
			//ゲーム描画を行う
			pRootJob->DrawSub();

			
			

			//描画終了
			Direct3D::EndDraw();
			
			//入力処理確認用
			if (Input::IsKeyUp(DIK_ESCAPE))
			{
				static int cnt = 0;
				cnt++;
				if (cnt >= 3)
				{
					PostQuitMessage(0);
				}
			}
		}
	}
	//モデルの全解放
	Model::AllRelease();
	Image::AllRelease();
	//Audio::Release();

	//Rootの解放
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);

	//Inputで使ったポインタの解放
	Input::Release();

	//Direct3Dオブジェクトとデバイスオブジェクトの解放
	Direct3D::Release();
	
	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)			//CALLBACK　<-なにかあったときに呼ばれる
{
	switch (msg)
	{
	case WM_DESTROY:		//ウィンドウを閉じた時
		PostQuitMessage(0);	//プログラム終了
		return 0;			//自分で何かした場合は0を返す
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);		//デフォルトウィンドウプロシージャ	デフォルトでいい場合はこれを返す
}