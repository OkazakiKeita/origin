#include "Camera.h"
#include "Direct3D.h"


//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"),target_(0,0,10)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	//今回は引数をいじらないので初期化に置いている
	//いじるときはUpdateへ
	//プロジェクション行列を作るための関数 第一:入れるとこ 第二:視野角,画角(縦) 第三:アスペクト比(横/高さ) 第四:カメラからどれくらいの距離で描画するか(近クリッピング面)
	//第五:カメラからどれくらいの距離まで描画するか(遠クリッピング面)
	//Perspective（遠近感）
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 1000.0f);
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{	//子として持った時に一緒にアップデートされるので良い
	//Updateが終わってからTransformが呼ばれているため
	//ワールドマトリクスが一つ前の情報になってしまっているのでワンテンポ遅れる
	//解消するためにTransformを呼んだ（少しムダ）
	Transform();
	D3DXVECTOR3 worldPosition, worldTarget;
	
	//自分のpositionベクトル,targetベクトルにワールド(親の移動回転拡大縮小)行列をかけることで
	//親に追従することができるようになる
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	D3DXMATRIX view;
	//ビュー行列を作るための関数 第一:入れるとこ　第二:カメラの位置(視点の位置) 第三:焦点,注視点(どこを見るか) 第四:カメラの上方向ベクトル(カメラの上がどっちか)(基本は010)
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//第一:どの行列として使うか	第二:行列
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}