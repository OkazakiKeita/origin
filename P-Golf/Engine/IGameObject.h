#pragma once
#include <d3dx9.h>	//Globalでもインクルードしてるけどとりあえず
#include<list>
#include <vector>
#include "Audio.h"

class Collider;

//インタフェース
class IGameObject
{
protected:
	IGameObject* pParent_;	//IGameObjectはインタフェースなのでポインタでないと使えない
	std::list<IGameObject*> childList_;	//子のリストを入れる
	std::string name_;			//文字列を入れる変数

	D3DXVECTOR3 position_;	//位置
	D3DXVECTOR3 rotate_;	//回転
	D3DXVECTOR3 scale_;		//拡大
	//D3DXVECTOR3 normal_;
	std::vector<D3DXVECTOR3> normal_;
	D3DXVECTOR3 wallPos_[30];

	//親から見た行列
	D3DXMATRIX localMatrix_; //状態(拡大変形回転)
	//世界から見た行列
	D3DXMATRIX worldMatrix_;

	//killmeの中で用いてupdateの時にtrueならオブジェクトを消す
	bool dead_;

	//SetColliderで用いる
	/*Collider *pCollider_;*/
	std::vector<Collider*> pCollider_;

	//オブジェクトの拡大回転移動等の変形を行う
	//ワールドマトリクスを設定する
	//戻り値 void型
	void Transform();

	//オブジェクトの名前を管理する
	std::string objectName_;

public:
	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	//基本はこれが呼ばれる
	//引数 第一:IgameObject*型 親の名前 第二:string型 オブジェクトの名前
	IGameObject(IGameObject* parent, const std::string& name);

	//デストラクタ
	//継承しているので子クラスが消えた時にはこっちのデストラクタが呼び出されてしまう
	//オーバーライド状態になっている
	//解消するためにvirtualにした
	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;

	//更新
	//各クラスのものを行う
	virtual void Update() = 0;

	//描画
	//各クラスのものを行う
	virtual void Draw() = 0;

	//開放
	//各クラスのものを行う
	virtual void Release() = 0;


	void UpdateSub();

	void DrawSub();

	void ReleaseSub();

	void KillMe();

	//Collider(当たり判定)をつける
	//引数 第一:D3DXVECTOR3型 中心 第二:float型 半径
	//戻り値 void型
	void SetSphereCollider(D3DXVECTOR3& center, float radius);

	//箱型の当たり判定をつける
	void SetBoxCollider(D3DXVECTOR3& center, D3DXVECTOR3& size);

	//当たっているかの確認を行う
	//引数 第一:IGameObject*型 当たり判定する相手
	//戻り値 void型
	void Collision(IGameObject* targetObject);

	//当たった時の処理を行う
	//引数 第一: IGameObject*型 当たったオブジェクト
	//戻り値 void型
	virtual void OnCollision(IGameObject* object, int num);

	virtual void OnCollision(IGameObject* object);


	//GameObjectの生成
	//クラスを引数のように扱えるようになる
	//templateはインラインで使ったほうが安全かも
	//CreateGameObject<class>(IgameObject* parent)
	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//オブジェクト作成
		T* p = new T(parent);

		//親のchildListに追加する
		parent->PushBackChild(p);
		//作ったオブジェクトのイニシャライズを行う
		p->Initialize();
		return p;
	}

	//childListに入れ込む処理
	//引数: IgameObject*型 入れるオブジェクト
	//戻り値 void型
	void IGameObject::PushBackChild(IGameObject* pobj);

	//positionのセッター
	//引数 D3DXVECTOR3型 position
	//戻り値 void型
	void SetPosition(D3DXVECTOR3 pos)
	{
		position_ = pos;
	}

	//positionのゲッター
	//引数 なし
	//戻り値 D3DXVECTOR3型
	D3DXVECTOR3 GetPosition()
	{
		return position_;
	}
	
	//rotateのセッター
	//引数 D3DXVECTOR3型 rotate
	//戻り値 void
	void SetRotate(D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	}
	
	//rotateのゲッター
	//引数 なし
	//戻り値 D3DXVECTOR3型
	D3DXVECTOR3 GetRotate()
	{
		return rotate_;
	}
	
	//scaleのセッター
	//引数 D3DXVECTOR3型 scale
	//戻り値 void
	void SetScale(D3DXVECTOR3 scale)
	{
		scale_ = scale;
	}

	//scaleのゲッター
	//引数 なし
	//戻り値 D3DXVECTOR3型
	D3DXVECTOR3 GetScale()
	{
		return scale_;
	}

	//objectnameのゲッター
	//引数 なし
	//戻り値 string型
	const std::string& GetObjectName()
	{
		return objectName_;
	}

	D3DXVECTOR3 GetNormal(int num)
	{
		return normal_[num];
	}

	int GetHModel(int hModel)
	{
		return hModel;
	}

	D3DXVECTOR3 GetwPos(int num)
	{
		return wallPos_[num];
	}
};

