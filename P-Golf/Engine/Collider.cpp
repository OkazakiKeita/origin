#include "Collider.h"
#include "../Wall.h"

Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius)
	:owner_(owner),center_(center),radius_(radius),IsBoxCollider_(false)
{
}

Collider::Collider(IGameObject * owner, D3DXVECTOR3 center, D3DXVECTOR3 size)
	:owner_(owner),center_(center),size_(size),IsBoxCollider_(true)
{
}


Collider::~Collider()
{
}

//誰のColiderなのかがわからないと判定がついてこないので置き去りにされてしまう
bool Collider::IsHit(Collider& target, int num)
{
	//箱型同士の当たり判定
	if (IsBoxCollider_ && target.IsBoxCollider_)
	{
		return IsHitBoxBox(target, num);
	}
	//球型同士の当たり判定
	else if ((IsBoxCollider_ == false) && (target.IsBoxCollider_ == false))
	{
		return IsHitBallBall(target, num);
	}
	//箱型と球型の当たり判定
	else
	{
		return IsHitBallBox(target, num);
	}



	////相手のベクトル引く自分のベクトル
	////移動するのでownerの位置も必要
	//D3DXVECTOR3 v = (center_ + owner_->GetPosition()) - (target.center_ + target.owner_->GetPosition());
	////ベクトルの長さを求める
	//float length = D3DXVec3Length(&v);
	////半径の和のほうが中心同士の距離よりも長ければ
	////=をつけないとぴったりの時に当たってないことになるのでつける
	//if (length <= radius_ + target.radius_)
	//{
	//	return true;
	//}

	return false;
}

bool Collider::IsHitBallBox(Collider& target, int num)
{
	D3DXVECTOR3 spherePos, boxPos , boxSize;
	float radius;
	
	if (IsBoxCollider_)
	{
		spherePos = target.center_ + target.owner_->GetPosition();
		radius = target.radius_;
		/*boxPos = center_ + owner_->GetPosition();*/
		boxPos = center_ /*+ owner_->GetwPos(num)*/;
		boxSize = size_;
	}
	else
	{
		spherePos = center_ + owner_->GetPosition();
		radius = radius_;
		boxPos = target.center_ + target.owner_->GetPosition();
		boxSize = target.size_;
	}

	/*if (((spherePos.x + radius > boxPos.x - boxSize.x / 2) && (spherePos.x - radius < boxPos.x + boxSize.x / 2)) &&
		((spherePos.y > boxPos.y - boxSize.y / 2) && (spherePos.y < boxPos.y + boxSize.y / 2)) && 
		((spherePos.z + radius > boxPos.z - boxSize.z / 2) && (spherePos.z - radius < boxPos.z + boxSize.z / 2)))
	{
		return true;
	}*/
	/*if (((spherePos.x > boxPos.x - boxSize.x / 2) && (spherePos.x < boxPos.x + boxSize.x / 2)) &&
		((spherePos.y + radius > boxPos.y - boxSize.y / 2) && (spherePos.y - radius < boxPos.y + boxSize.y / 2)) &&
		((spherePos.z + radius > boxPos.z - boxSize.z / 2) && (spherePos.z - radius < boxPos.z + boxSize.z / 2)))
	{
		return true;
	}*/

	//if (IsInRadius(spherePos, boxPos, boxSize, radius))
	//{
	//	return true;
	//}

	if ( ((spherePos.x + radius > boxPos.x - boxSize.x / 2) && (spherePos.x - radius < boxPos.x + boxSize.x / 2)) &&
		 ((spherePos.y + radius > boxPos.y - boxSize.y / 2) && (spherePos.y - radius < boxPos.y + boxSize.y / 2)) &&
		 ((spherePos.z + radius > boxPos.z - boxSize.z / 2) && (spherePos.z - radius < boxPos.z + boxSize.z / 2)) )
	{
		return true;
		if (IsInRadius(spherePos, boxPos, boxSize, radius))
		{
			return true;
		}
	}

	return false;
}

bool Collider::IsHitBallBall(Collider& target, int num)
{
	//相手のベクトル引く自分のベクトル
	//移動するのでownerの位置も必要
	D3DXVECTOR3 v = (center_ + owner_->GetPosition()) - (target.center_ + target.owner_->GetPosition());
	//ベクトルの長さを求める
	float length = D3DXVec3Length(&v);
	//半径の和のほうが中心同士の距離よりも長ければ
	//=をつけないとぴったりの時に当たってないことになるのでつける
	if (length <= radius_ + target.radius_)
	{
		return true;
	}

	return false;
}

bool Collider::IsHitBoxBox(Collider& target, int num)
{
	D3DXVECTOR3 boxAPos = center_ + owner_->GetPosition();
	D3DXVECTOR3 boxBPos = target.center_ + target.owner_->GetPosition();

	if ((boxAPos.x + size_.x / 2.0f > boxBPos.x - target.size_.x / 2.0f) &&
		(boxAPos.x - size_.x / 2.0f < boxBPos.x + target.size_.x / 2.0f) &&
		(boxAPos.y + size_.y / 2.0f > boxBPos.y - target.size_.y / 2.0f) &&
		(boxAPos.y - size_.y / 2.0f < boxBPos.y + target.size_.y / 2.0f) &&
		(boxAPos.z + size_.z / 2.0f > boxBPos.z - target.size_.z / 2.0f) &&
		(boxAPos.z - size_.z / 2.0f < boxBPos.z + target.size_.z / 2.0f))
	{
		return true;
	}

	return false;
}

bool Collider::IsInRadius(D3DXVECTOR3 spherepos, D3DXVECTOR3 boxpos, D3DXVECTOR3 boxsize, float radius)
{
	for (int n = 0; n < 8; n++)
	{
		D3DXVECTOR3 vertexpos;
		vertexpos.x = boxsize.x * vertexIndex[n].x;
		vertexpos.y = boxsize.y * vertexIndex[n].y;
		vertexpos.z = boxsize.z * vertexIndex[n].z;

		D3DXVECTOR3 v = spherepos - (boxpos + (vertexpos));
		//ベクトルの長さを求める
		float length = D3DXVec3Length(&v);
		//半径の和のほうが中心同士の距離よりも長ければ
		//=をつけないとぴったりの時に当たってないことになるのでつける
		if (length <= radius)
		{
			return true;
		}
	}
	return false;
}


