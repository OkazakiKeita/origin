#pragma once
#include <d3dx9.h>		//他のところでGlobal.hをincludeするのでここに書けば他のところでこれを個別にincludeする必要がなくなる
#include <assert.h>
#include"Input.h"
#include"IGameObject.h"
#include"SceneManager.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_RELEASSE(p) if(p != nullptr){ p->Release(); p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}

struct Global	//Grobal型の構造体
{
	int screenWidth;	//スクリーン幅を入れる
	int screenHeight;	//スクリーン高さを入れる
};
extern Global g;	//まとめてexternできる（どこかで定義すればその値を使える）	※メモに詳細あり