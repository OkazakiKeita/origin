#include "Sprite.h"
#include "Direct3D.h"


Sprite::Sprite():
	pSprite_(nullptr),pTexture_(nullptr)	//こっちに書くと作りつつ入力されるから少し早い
{
	//中に書くと箱作った後に入力する形になる
}


Sprite::~Sprite()
{
	SAFE_RELEASSE(pTexture_);
	SAFE_RELEASSE(pSprite_);
}

void Sprite::Load(const char *FileName)				//変化しないのでconst
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);	//第一（どこで使うか）	Direct3D::pDeviceをヘッダで宣言しているので使える
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, FileName,				//使える画像サイズは2の乗数にしておかないと変な表示になる。幅は1024にしとくといいかも
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);			//最後にテクスチャのアドレス
	assert(pTexture_ != nullptr);

}

void Sprite::Draw(const D3DXMATRIX& matrix, RECT* rect, float alpha)
{
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, rect, nullptr, nullptr, D3DXCOLOR(1, 1, 1, alpha));
	pSprite_->End();
}

//テクスチャのサイズを取得
D3DXVECTOR2 Sprite::GetTextureSize()
{
	D3DXVECTOR2 size;
	D3DSURFACE_DESC d3dds;
	pTexture_->GetLevelDesc(0, &d3dds);
	size.x = (FLOAT)d3dds.Width;
	size.y = (FLOAT)d3dds.Height;

	return size;
}