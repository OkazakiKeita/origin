#pragma once
#include "Global.h"

class Quad
{
	struct Vertex					//D3DFVF系の宣言を確認してその順番に作る
	{
		D3DXVECTOR3 pos;			//位置情報
		D3DXVECTOR3 normal;			//法線情報
		D3DXVECTOR2 uv;				//uv情報（二次元情報）
	};

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;

	LPDIRECT3DTEXTURE9 pTexture_;
	D3DMATERIAL9 material_;

public:
	Quad();
	~Quad();

	virtual void Load(const char* FileName);
	virtual void Draw(const D3DXMATRIX &matrix);
};