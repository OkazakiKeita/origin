#include "SceneManager.h"
#include "../PlayScene.h"
#include "../TitleScene.h"
#include "../ExplanationScene.h"
#include "../ResultScene.h"
//全シーンをインクルードしないと管理できない

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_TITLE;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_TITLE;
int		 SceneManager::patCnt_ = -1;
IGameObject* SceneManager::pCurrentScene = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene = CreateGameObject<TitleScene>(this);
}

//更新
void SceneManager::Update()
{
	//currentとnextが違うときにシーンを切り替える
	if (currentSceneID_ != nextSceneID_)
	{
		//子は一つしかないから繰り返す必要はない
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		//空にはしたけど解放はしてないので
		SAFE_DELETE(*scene);

		//SceneManagerのchildListに虚無がはいったままで
		//listにプレイシーンの残骸が残っているから
		//childlistをまっさらにする
		childList_.clear();

		switch (nextSceneID_)
		{
		case SCENE_ID_TITLE:
			pCurrentScene = CreateGameObject<TitleScene>(this);
			break;
		case SCENE_ID_EXPLANATION:
			pCurrentScene = CreateGameObject<ExplanationScene>(this);
			break;
		case SCENE_ID_PLAY: 
			pCurrentScene = CreateGameObject<PlayScene>(this);
			break;
		case SCENE_ID_RESULT:
			pCurrentScene = CreateGameObject<ResultScene>(this);
			((ResultScene*)pCurrentScene)->SetPatCnt(patCnt_);
		}
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//次のシーンの登録
void SceneManager::ChangeScene(SCENE_ID next, int patcnt)
{
	nextSceneID_ = next;
	patCnt_ = patcnt;
}
