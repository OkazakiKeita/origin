#include<vector>
#include "Image.h"

namespace Image
{
	std::vector<ImageData*> dataList_;

	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
 		assert(pData != nullptr);

		pData->fileName = fileName;



		//存在するかのフラグ
		bool isExist = false;

		//dataList内の探索
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//同名のファイルがロードされているかの確認
 			if (dataList_[i]->fileName == fileName)
			{
				//すでにロードされてるSpriteの情報が入ったアドレスをコピーする
				pData->psprite = dataList_[i]->psprite;
				isExist = true;
				break;
			}
		}

		//見つからなかった時に新たにロードする
		if (isExist == false)
		{
			//Spriteもnewしてあげないとロード時にエラー
			pData->psprite = new Sprite;
			assert(pData->psprite != nullptr);

			//引数の.c_str()でchar型に変換している
			pData->psprite->Load(fileName.c_str());
		}




		//dataListに追加
		dataList_.push_back(pData);

		int handle = dataList_.size() - 1;
		ResetRect(handle);

		//1番目の情報は0に、2番目の情報は1に入るので
		//サイズから1を引いた値を渡すとよい
		return handle;
	}

	void Draw(int handle, float alpha)
	{
		dataList_[handle]->psprite->Draw(dataList_[handle]->matrix, &dataList_[handle]->rect, alpha);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		//行列の登録
		dataList_[handle]->matrix = matrix;
	}

	void SetRect(unsigned int handle, int x, int y, int width, int height)
	{
		if (handle < 0 || handle >= dataList_.size())
		{
			return;
		}

		dataList_[handle]->rect.left = x;
		dataList_[handle]->rect.top = y;
		dataList_[handle]->rect.right = x + width;
		dataList_[handle]->rect.bottom = y + height;
	}

	//切り抜き範囲をリセット（画像全体を表示する）
	void ResetRect(unsigned int handle)
	{
		if (handle < 0 || handle >= dataList_.size())
		{
			return;
		}

		D3DXVECTOR2 size = dataList_[handle]->psprite->GetTextureSize();

		dataList_[handle]->rect.left = 0;
		dataList_[handle]->rect.top = 0;
		dataList_[handle]->rect.right = (LONG)size.x;
		dataList_[handle]->rect.bottom = (LONG)size.y;

	}

	void Release(int handle)
	{
		//存在するかのフラグ管理
		bool isExist = false;

		//同じのを使ってるか調べてなかったらdelete
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//比べるのはpspriteでもfileNameでもよい
			//総当たりなので i と handle が同じとき(自分と自分)
			//を比べないようにする
			//
			if (i != handle && dataList_[i] != nullptr &&
				dataList_[i]->psprite == dataList_[handle]->psprite)
			{
				isExist = true;
				break;
			}
		}

		//他に誰も参照してないとき
		if (isExist == false)
		{
			//fbxの情報を削除する
			SAFE_DELETE(dataList_[handle]->psprite);
		}

		//fbx以外の情報(ファイル名、行列を削除する)
		SAFE_DELETE(dataList_[handle]);
	}

	//作ったものをすべて解放する
	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//まだ削除されていなければ
			if (dataList_[i] != nullptr)
			{
				//解放処理を行う
				//0から順番に解放
				Release(i);
			}
		}
		//dataList_の中身をまっさらにする
		dataList_.clear();
	}

};