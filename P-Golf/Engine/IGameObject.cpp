#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"




//親も名前も知らない
IGameObject::IGameObject() : IGameObject(nullptr, "")
{
}

//親のみ知ってる、名前は知らない
IGameObject::IGameObject(IGameObject * parent) : IGameObject(parent, "")
{
}

//基本的にこのコンストラクタを使う。親と名前知ってる
IGameObject::IGameObject(IGameObject * parent, const std::string & name) 
		: pParent_(parent),name_(name), position_(D3DXVECTOR3(0, 0, 0)), rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),	//親と名前はわかるので設定
		dead_(false),/*pCollider_(nullptr),*/objectName_(name)
{
}

//pParentはここでデリートすると子が死んだときに一緒に死んでしまうのでしないようにする
IGameObject::~IGameObject()
{
	//ここでいいのか
	/*SAFE_DELETE(pCollider_);*/
	for (unsigned int i = 0; i < pCollider_.size(); i++)
	{
		SAFE_DELETE(pCollider_[i]);
	}
}

void IGameObject::Transform()
{
	D3DXMATRIX matT, matRx, matRy, matRz, matS;
	//移動行列
	//変形
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);

	//回転
	D3DXMatrixRotationX(&matRx, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRy, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRz, D3DXToRadian(rotate_.z));

	//スケールの変更
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//このオブジェクトの拡大変形回転の状態を保存
	/*localMatrix_ = matS * matRy * matRx * matRz * matT;*/
	localMatrix_ = matS * matRx * matRy * matRz * matT;

	//回転が先に行われる
	//rootjobにおいて親がいないため親のワールドマトリクスがわからない
	if (pParent_ != nullptr)
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_ ;
	}
	else
	{
		worldMatrix_ = localMatrix_;
	}
}

void IGameObject::UpdateSub()
{
	Update();
	Transform();

	//自分に当たり判定があるなら当たっているのか判定する
	for (auto itr = pCollider_.begin(); itr != pCollider_.end(); itr++)
	{
		Collision(SceneManager::GetCurrentScene());
	}
	/*if (pCollider_ != nullptr)
	{
		Collision(SceneManager::GetCurrentScene());
	}*/



	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//ポインタ(it)のさしてる中身を表示するために*が必要
		//()でくくらないと参照がおかしくなるのでitの参照だと明記する
		(*it)->UpdateSub();
	}
	for (auto it = childList_.begin(); it != childList_.end();)
	{
		//イテレータの指すもののフラグが立っていたら
		//オブジェクトを削除する
		if ((*it)->dead_ == true)
		{
			//ちゃんと子も削除するために
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			
			//居なくなる時に次のアドレスを教えてくれるので
			//戻り値としてitを受け取らなければならない
			//次のアドレスになっているので++しなくてよい
			it = childList_.erase(it);
			
		}
		//見つからなかったときは次の値にする
		else
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//ポインタ(it)のさしてる中身を表示するために*が必要
		//()でくくらないと参照がおかしくなるのでitの参照だと明記する
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//ポインタ(it)のさしてる中身を表示するために*が必要
		//()でくくらないと参照がおかしくなるのでitの参照だと明記する
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::SetSphereCollider(D3DXVECTOR3 & center, float radius)
{
	//当たり判定を作成する　owner->this ,center->center, radius->radius
	//pCollider_ =new Collider(this, center, radius);
	/*if (pCollider_ == nullptr)
	{
		pCollider_ =new Collider(this, center, radius);
	}*/
	pCollider_.push_back(new Collider(this, center, radius));
	//else
	//{
	//	//判定を追加する処理
	//}
}

void IGameObject::SetBoxCollider(D3DXVECTOR3& center, D3DXVECTOR3& size)
{
	pCollider_.push_back(new Collider(this, center, size));
	//if (pCollider_ == nullptr)
	//{
	//	pCollider_ = new Collider(this, center, size);
	//}
	//else
	//{
	//	//追加する処理
	//}
}

void IGameObject::Collision(IGameObject* targetObject)
{
	////targetObjectに当たり判定があるときに判定する
	//if (targetObject != this &&					//当たった相手が自分ではない
	//	targetObject->pCollider_ != nullptr &&	//targetに判定がある
	//	pCollider_->IsHit(*targetObject->pCollider_))	//当たっているのか判定
	//{
	//	//当たった時の処理
	//	this->OnCollision(targetObject, );
	//}
	//targetObjectに当たり判定があるときに判定する
	int num = 0;
	for (auto tItr = targetObject->pCollider_.begin(); tItr != targetObject->pCollider_.end(); tItr++)
	{
		if (targetObject != this &&					//当たった相手が自分ではない
			//targetObject->pCollider_ != nullptr &&	//targetに判定がある
			//(this.p)->IsHit(*(*tItr)))	//当たっているのか判定
			(*tItr)->IsHit(*this->pCollider_[0], num))
		{
			//当たった時の処理
			this->OnCollision(targetObject, num);
			this->OnCollision(targetObject);
		}
		num++;
	}
	//子との判定を見たい
	for (auto itr = targetObject->childList_.begin();
		itr != targetObject->childList_.end(); itr++)
	{
		//再帰処理で対象の子との総当たりを行う
		Collision(*itr);
	}
	//自分(シーン内の1オブジェクトからそのシーン内のすべてのオブジェクトに対して判定する)
}

void IGameObject::OnCollision(IGameObject * object, int num)
{
}

void IGameObject::OnCollision(IGameObject * object)
{
}

//チャイルドリストに突っ込む
void IGameObject::PushBackChild(IGameObject* pobj)
{
	childList_.push_back(pobj);
}
