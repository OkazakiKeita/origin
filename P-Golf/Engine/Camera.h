#pragma once
#include "IGameObject.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	//焦点
	D3DXVECTOR3 target_;
	//カメラの傾きが必要な時は上方向入れる変数も用意する

public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//targetのセッター
	//引数 D3DXVECTOR3型 target
	//戻り値　void型
	void SetTarget(D3DXVECTOR3 targ)
	{
		target_ = targ;
	}

	D3DXVECTOR3 GetTarget()
	{
		return target_;
	}
};