#include "Fbx.h"
#include "Direct3D.h"


Fbx::Fbx():pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr), pmaterial_(nullptr),
		   pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr), vertexCount_(0),
		   polygonCount_(0), indexCount_(0), materialCount_(0), polygonCountOfMaterial_(nullptr)//ポインタの初期化,配列の初期化
{
}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(pmaterial_);
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);

	//テクスチャ,インデックスバッファーの解放
	for (int i = 0; i < materialCount_; i++)	//ポインタの先のポインタを解放
	{
		SAFE_RELEASSE(pTexture_[i]);
		SAFE_RELEASSE(ppIndexBuffer_[i]);
	}
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_DELETE_ARRAY(ppIndexBuffer_);			//元を解放

	SAFE_RELEASSE(pVertexBuffer_);//解放	作った順と逆順に開放
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char*  FileName)
{
	pManager_ = FbxManager::Create();					//manegerの作成
	pImporter_ = FbxImporter::Create(pManager_, "");	//インポーターの作成、第一pmaneger、第二名前つけるときに名前
	pScene_ = FbxScene::Create(pManager_, "");			//シーンの作成、　　　第一pmaneger、第二名前つけるときに名前
	pImporter_->Initialize(FileName);					
	pImporter_->Import(pScene_);						//開いたデータをシーンに入れる
	pImporter_->Destroy();								//解放処理

	//デフォルトのディレクトリの保存
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);	//カレントディレクトリの保存


	//カレントディレクトリの変更
	char dir[MAX_PATH];
	_splitpath_s(FileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);	//パスの入手
	SetCurrentDirectory(dir);							//以降はこのパスを参照

	FbxNode* rootNode = pScene_->GetRootNode();			//ルートノードの情報を取得
	int childCount = rootNode->GetChildCount();			//ルートノードの子の数をカウントする
	assert(childCount >= 0);
	for (int i = 0; childCount > i; i++)
	{
		//ノードの中身チェック
		CheckNode(rootNode->GetChild(i));
	}

	//参照先を戻す
	SetCurrentDirectory(defaultCurrentDir);
}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);		//変形の適用	ワールド行列（位置や回転、変形などの情報を持っている） 	
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1/*D3DFVF_DIFFUSE*/);			//ビット演算で位置と情報をまとめる
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));	//表示したいデータが入ってる場所を指定する

	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);				//使用するテクスチャの設定	第一テクスチャ番号　第二使用するテクスチャ
		Direct3D::pDevice->SetMaterial(&pmaterial_[i]);					//使用するマテリアルの設定
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);	//第一使い方	第四頂点データの数	第六出したいポリゴンの数
	}

}

void Fbx::RayCast(RayCastData & data, const D3DXMATRIX & matrix)
{
	float minDistance = 9999.0f;

	//頂点バッファをロック
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < (unsigned)materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < (unsigned)polygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			//ローカル座標なのでそのままは使えない
			D3DXVECTOR3 v0, v1, v2, normal;
			v0 = vCopy[iCopy[j * 3 + 0]].pos;
			v1 = vCopy[iCopy[j * 3 + 1]].pos;
			v2 = vCopy[iCopy[j * 3 + 2]].pos;

			//v0,v1,v2にワールド行列を掛け合わせてワールド座標に変換
			D3DXVec3TransformCoord(&v0, &v0, &matrix);
			D3DXVec3TransformCoord(&v1, &v1, &matrix);
			D3DXVec3TransformCoord(&v2, &v2, &matrix);

			// ※ここで、D3DXIntersectTri関数を使う
			//IntersectTri関数の引数 1,2,3 : ポリゴンの座標、 4 : レイの発射位置、 5レイの方向、 6,7 : 当たった時の座標、 8 : 距離	引数6,7,8は登録する場所を入れる
			//戻り値 bool型　当たったらtrue
			if (data.dist < minDistance)
			{
				data.hit = D3DXIntersectTri(&v0, &v1, &v2, &data.startPos, &data.rayDir, nullptr, nullptr, &data.dist);
				if (data.hit)
				{
					minDistance = data.dist;

					D3DXVECTOR3 vecA = v1 - v0;
					D3DXVECTOR3 vecB = v2 - v0;

					D3DXVec3Cross(&data.normal, &vecA, &vecB);
					D3DXVec3Normalize(&data.normal, &data.normal);

					if (v0.y < v1.y)
					{
						if (v0.y < v2.y)
						{
							data.lowerPos = v0;
						}
						else
						{
							data.lowerPos = v2;
						}
					}
					else
					{
						if (v1.y < v2.y)
						{
							data.lowerPos = v1;
						}
						else
						{
							data.lowerPos = v2;
						}
					}

				}
			}
			//hitがtrueのたびに呼び出す
			//今のdistanceと前のdistanceを見て小さければ入れ替え
		}

		//インデックスバッファ使用終了
		ppIndexBuffer_[i]->Unlock();
	}
}


void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//頂点の取得
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	vertexCount_ = pMesh->GetControlPointsCount();			//頂点の個数を取得
	Vertex* vertexList = new Vertex[vertexCount_];			//配列を作る Vertex型でvertexCount_の数だけ
	assert(vertexList != nullptr);
	polygonCount_ = pMesh->GetPolygonCount();				//ポリゴンの個数を取得
	indexCount_ = pMesh->GetPolygonVertexCount();			//インデックスの個数を取得(ポリゴン数の3倍になる)

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}
	
	//法線の取得
	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);	//ポリゴンを作るときの最初の頂点を取得
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);	//i番目のポリゴンのj番目の頂点の法線情報をNormalに入れる
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//uv情報の取得
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	SAFE_DELETE_ARRAY(vertexList);

	//インデックスの取得
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	assert(ppIndexBuffer_ != nullptr);

	polygonCountOfMaterial_ = new int[materialCount_];
	assert(polygonCountOfMaterial_ != nullptr);
	

	for (int i = 0; i < materialCount_; i++)										//マテリアルの個数分ループ
	{
		int* indexList = new int[indexCount_];
		assert(indexList != nullptr);

		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)					//ポリゴンの数だけループ
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);		//そのポリゴンのマテリアルIDを取得する
			if (materialID == i)		//マテリアルごとに分割する
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		polygonCountOfMaterial_[i] = count / 3;			//インデックスの数/3 マテリアルごとのポリゴン数を求める

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList)
	}
}

void Fbx::CheckNode(FbxNode* pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();		//マテリアルの個数を数えて
		pmaterial_ = new D3DMATERIAL9[materialCount_];	//その数だけ配列を作成
		assert(pmaterial_ != nullptr);
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];	//マテリアルの数だけ作成
		assert(pTexture_ != nullptr);

		//作った配列の中身をすべてnullptrで初期化する
		ZeroMemory(pTexture_, sizeof(LPDIRECT3D9) * materialCount_);		//ptexture_をサイズ分


		//マテリアルカウント分マテリアル情報を読み取る
		for (int i = 0; i < materialCount_; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);

			FbxDouble3 diffuse = lambert->Diffuse;	//拡散反射光
			FbxDouble3 ambient = lambert->Ambient;	//環境光

			ZeroMemory(&pmaterial_[i], sizeof(D3DMATERIAL9));

			pmaterial_[i].Diffuse.r = (float)diffuse[0];
			pmaterial_[i].Diffuse.g = (float)diffuse[1];
			pmaterial_[i].Diffuse.b = (float)diffuse[2];
			pmaterial_[i].Diffuse.a = 1.0f;					//透明度　透明0〜1不透明    テクスチャ貼るのであんまり使わないかな

			pmaterial_[i].Ambient.r = (float)ambient[0];
			pmaterial_[i].Ambient.g = (float)ambient[1];
			pmaterial_[i].Ambient.b = (float)ambient[2];
			pmaterial_[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);	//テクスチャに関する情報取得
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);					//ファイルに関する情報を取得
			if (textureFile == nullptr)
			{
				//テクスチャが張られてなければ
				pTexture_[i] = nullptr;
			}
			else
			{
				//貼られていれば
				const char* textureFileName = textureFile->GetFileName();
				
				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);			//第一に第二を出力	文字列をつなげて保存することができる

				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
					D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);
				assert(pTexture_[i] != nullptr);
				int a = 0;
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

