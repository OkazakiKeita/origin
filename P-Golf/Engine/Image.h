#pragma once
#include<string>
#include"Sprite.h"

namespace Image
{
	//ImageDataの構造体
	struct ImageData
	{
		std::string fileName;	//文字列(ファイル名を入れる)
		Sprite*	psprite;	//Spriteの情報
		RECT rect;			//切り抜き範囲
		D3DXMATRIX matrix;		//行列

		//c++だと構造体でもコンストラクタが使える
		//コンストラクタ
		ImageData() :fileName(""), psprite(nullptr)
		{
			//行列の初期化は中に書かないといけない
			//単位行列として初期化(特に何もしない)
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName);
	void Draw(int handle, float alpha = 1.0f);

	//第一引数番目のモデルのmatrixに第二引数のmatrixをセットする
	//第一引数: int型 モデル番号
	//第二引数: D3DXMATRIX型 行列
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//切り抜き範囲を指定する
	void SetRect(unsigned int handle, int x, int y, int width, int height);

	void ResetRect(unsigned int handle);

	//解放処理を行う関数
	//引数
	//戻り値
	void Release(int handle);
	//すべて解放する
	//引数 なし
	//戻り値 void型
	void AllRelease();
};