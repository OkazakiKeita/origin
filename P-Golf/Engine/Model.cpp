#include<vector>
#include "Model.h"

namespace Model
{
	std::vector<ModelData*> dataList_;

	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		assert(pData != nullptr);

		pData->fileName = fileName;
		
		//存在するかのフラグ
		bool isExist = false;

		//dataList内の探索
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//同名のファイルがロードされているかの確認
			if (dataList_[i]->fileName == fileName)
			{
				//すでにロードされてるFBXの情報が入ったアドレスをコピーする
				pData->pFbx = dataList_[i]->pFbx;
				isExist = true;
				break;
			}
		}

		//見つからなかった時に新たにロードする
		if (isExist == false)
		{
			//Fbxもnewしてあげないとロード時にエラー
			pData->pFbx = new Fbx;
			assert(pData->pFbx != nullptr);

			//引数の.c_str()でchar型に変換している
			pData->pFbx->Load(fileName.c_str());
		}

		//dataListに追加
		dataList_.push_back(pData);

		//1番目の情報は0に、2番目の情報は1に入るので
		//サイズから1を引いた値を渡すとよい
		return dataList_.size() - 1;
	}

	void Draw(int handle)
	{
		dataList_[handle]->pFbx->Draw(dataList_[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		//行列の登録
		dataList_[handle]->matrix = matrix;
	}

	void Release(int handle)
 	{
		//存在するかのフラグ管理
		bool isExist = false;

		//同じのを使ってるか調べてなかったらdelete
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//比べるのはpFbxでもfileNameでもよい
			//総当たりなので i と handle が同じとき(自分と自分)
			//を比べないようにする
			//
			if (i != handle && dataList_[i] != nullptr &&
				dataList_[i]->pFbx == dataList_[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		//他に誰も参照してないとき
		if (isExist == false)
		{
			//fbxの情報を削除する
			SAFE_DELETE(dataList_[handle]->pFbx);
		}

		//fbx以外の情報(ファイル名、行列を削除する)
		SAFE_DELETE(dataList_[handle]);
	}

	//作ったものをすべて解放する
	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//まだ削除されていなければ
			if (dataList_[i] != nullptr)
			{
				//解放処理を行う
				//0から順番に解放
				Release(i);
			}
		}
		//dataList_の中身をまっさらにする
		dataList_.clear();
	}

	void RayCast(int handle, RayCastData & data)
	{
		dataList_[handle]->pFbx->RayCast(data, dataList_[handle]->matrix);
	}

};