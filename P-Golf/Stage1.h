#pragma once
#include "Engine/IGameObject.h" 
#include "PatCount.h"
#include "Wall.h"
#include "Flag.h"
#include "Tree.h"

//ステージを管理するクラス
class Stage1 : public IGameObject
{
	//モデル、クラス
	int hModel_;
	Flag* pFlag_;
	Wall* pWall_;
	Tree* pTree_;

public:
	//コンストラクタ
	Stage1(IGameObject* parent);

	//デストラクタ
	~Stage1();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ステージのモデル番号取得
	int GetStageHandle()
	{
		return hModel_;
	}
	//フラッグ情報取得
	Flag* GetFlag()
	{
		return pFlag_;
	}
};