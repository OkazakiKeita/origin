#include <iostream>
#include <fstream>
#include <sstream>


#include "Wall.h"
#include "Engine/Model.h"


enum WALLDATA
{
	POS_X,
	POS_Y,
	POS_Z,
	ROT_Y,
	WIDTH,
	HEIGHT,
	DEPTH
};

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall"), hModel_()
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	hModel_ = Model::Load("Data/Model/Wall.fbx");
	hModel_in_ = Model::Load("Data/Model/InternalWall.fbx");
	
	SettingColiderPos();
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);

	Model::SetMatrix(hModel_in_, worldMatrix_);
	Model::Draw(hModel_in_);
}


//開放
void Wall::Release()
{
}

//コライダーの設定
void Wall::SettingColiderPos()
{
	//ファイル読み込みで行う
	const char* fileName = "Data/WallPos.txt";
	std::ifstream ifs(fileName);
	std::string data;
	int count = 0;

	//法線方向
	D3DXVECTOR3 normal  = D3DXVECTOR3(0, 0, 1);
	D3DXVECTOR3 normal2 = D3DXVECTOR3(1, 0, 0);

	//読み込み開始
	while (std::getline(ifs, data))
	{
		//コンマで分ける
		std::vector<std::string> separate = GetComma(data, ',');

		//壁の位置登録
		wallPos_[count] = D3DXVECTOR3(std::stof(separate.at(POS_X)), std::stof(separate.at(POS_Y)), std::stof(separate.at(POS_Z)));

		//回転確認して法線方向登録
		if (std::stof(separate.at(ROT_Y)) == 0)
		{
			wallSize_[count] = D3DXVECTOR3(std::stof(separate.at(WIDTH)), std::stof(separate.at(HEIGHT)), std::stof(separate.at(DEPTH)));
			normal_.push_back(normal);
		}
		else
		{
			wallSize_[count] = D3DXVECTOR3(std::stof(separate.at(DEPTH)), std::stof(separate.at(HEIGHT)), std::stof(separate.at(WIDTH)));
			normal_.push_back(normal2);
		}

		//コライダー登録
		SetBoxCollider(wallPos_[count], wallSize_[count]);
	}
}

//データ分割
std::vector<std::string> Wall::GetComma(std::string & input, char comma)
{
	//データ登録場所
	std::istringstream stream(input);
	//取り出し場所
	std::string field;
	//結果格納用
	std::vector<std::string> result;

	//分割
	while (std::getline(stream, field, comma))
	{
		//格納
		result.push_back(field);
	}

	return result;
}