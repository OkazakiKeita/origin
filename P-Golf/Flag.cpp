#include "Flag.h"
#include "Engine/Model.h"
#include "Stage1.h"

const float FLAG_SIZE = 0.3f;
const D3DXVECTOR3 FLAG_POS = D3DXVECTOR3(0, 0, 0);

//コンストラクタ
Flag::Flag(IGameObject * parent)
	:IGameObject(parent, "Flag"), hModel_(-1)
{
}

//デストラクタ
Flag::~Flag()
{
}

//初期化
void Flag::Initialize()
{
	hModel_ = Model::Load("Data/Model/Flag.Fbx");
	assert(hModel_ >= 0);
	SetSphereCollider(D3DXVECTOR3(0, 1, 0), FLAG_SIZE);

	position_ = FLAG_POS;
	FollowGround();
}

//更新
void Flag::Update()
{
}

//描画
void Flag::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Flag::Release()
{
}

//地面の高さに合わせる
void Flag::FollowGround()
{
	RayCastData raydata;
	raydata.startPos = position_;
	raydata.startPos.y = 0;
	raydata.rayDir = D3DXVECTOR3(0, -1, 0);

	int hStageModel = ((Stage1*)pParent_)->GetStageHandle();

	Model::RayCast(hStageModel, raydata);
	if (raydata.hit)
	{
		position_.y = -raydata.dist;
	}
}