#include "Power.h"
#include "Engine/Image.h"

const float POW_GAGE_UP = 5.62f;
const unsigned int RECT_WIDTH = 125;
const unsigned int RECT_HEIGHT = 1024;

//コンストラクタ
Power::Power(IGameObject * parent)
	:IGameObject(parent, "Power"),hPictPower_(-1),hpictBase_(-1),pow_(0)
{
}

//デストラクタ
Power::~Power()
{
}

//初期化
void Power::Initialize()
{
	hPictPower_ = Image::Load("Data/Image/PowerBarBase.png");
	hpictBase_ = Image::Load("Data/Image/PowerBase.png");
}

//更新
void Power::Update()
{
	pow_ = (int)(pow_ * POW_GAGE_UP);
}

//描画
void Power::Draw()
{
	Image::Draw(hpictBase_);

	Image::SetRect(hPictPower_, 0, 0, RECT_WIDTH + pow_, RECT_HEIGHT);
	Image::Draw(hPictPower_);
}

//開放
void Power::Release()
{
}