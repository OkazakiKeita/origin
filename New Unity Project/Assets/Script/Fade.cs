﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Fade : MonoBehaviour
{

    public float speed = 0.01f;
    public float keepTime = 2.0f;
    Color color;
    float alpha;
    float red, green, blue;
    int alphaState;
    private const int ALPHA_UP = 0;
    private const int ALPHA_KEEP = 1;
    private const int ALPHA_DOWN = 2;

    // Start is called before the first frame update
    void Start()
    {
        alpha = gameObject.GetComponent<Image>().color.a;
        red = GetComponent<Image>().color.r;
        green = GetComponent<Image>().color.g;
        blue = GetComponent<Image>().color.b;
        alphaState = ALPHA_DOWN;
    }

    // Update is called once per frame
    void Update()
    {
        //黒のパネルのカラー設定
        GetComponent<Image>().color = new Color(red, green, blue, alpha);
        
        switch (alphaState)
        {
            case ALPHA_DOWN:
                AlphaDown();
                if(alpha <= 0.0f)
                {
                    alphaState = ALPHA_KEEP;
                }
                break;
            case ALPHA_KEEP:
                Invoke("ChangeState", keepTime);
                break;
            case ALPHA_UP:
                AlphaUp();
                if(alpha >= 1.0f)
                {
                    SceneManager.LoadScene("Title");
                }
                break;
        }
    }

    void AlphaDown()
    {
        alpha -= speed;
    }

    void AlphaUp()
    {
        alpha += speed;
    }
    
    void ChangeState()
    {
        alphaState = ALPHA_UP;
    }
}
