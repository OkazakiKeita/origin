﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleBackground : MonoBehaviour
{
    public Image image;
    private Sprite sprite;
    // Start is called before the first frame update
    void Start()
    {
        sprite = Resources.Load<Sprite>("TitleBackground");
        image = this.GetComponent<Image>();
        image.sprite = sprite;
        //gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/Station");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
